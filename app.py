from flask import Flask, render_template
import requests
import os

app = Flask(__name__)

# Retrieve API key and weather data URL from environment variables
api_key = os.environ.get('WEATHER_API_KEY')
weather_url = os.environ.get('WEATHER_API_URL')

@app.route('/')
def homepage():
    return f'Welcome, <your-name>!'

@app.route('/dashboard')
def dashboard():
    # Fetch weather data from the external API
    try:
        response = requests.get(weather_url, params={'apikey': api_key})
        weather_data = response.json()
        # Process and render the weather data as needed
        return render_template('dashboard.html', weather_data=weather_data)
    except Exception as e:
        return f'Error fetching weather data: {str(e)}'

if __name__ == '__main__':
    app.run(debug=True)
