From python:3.7-alpine

WORKDIR /

COPY ..

RUN pip install --no-dir-cache --upgrade pip
RUN pip install --no-dir-cache -r requirements.txt

EXPOSE 5000

CMD ["python", "-u", "main.py"]
